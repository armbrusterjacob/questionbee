webpack --mode development ./qb/frontend/src/quiz_create.js --output ./qb/frontend/static/frontend/quiz_create.js
webpack --mode development ./qb/frontend/src/quiz_list.js --output ./qb/frontend/static/frontend/quiz_list.js
webpack --mode development ./qb/frontend/src/quiz_edit.js --output ./qb/frontend/static/frontend/quiz_edit.js
webpack --mode development ./qb/frontend/src/quiz_results.js --output ./qb/frontend/static/frontend/quiz_results.js
webpack --mode development ./qb/frontend/src/user_detail.js --output ./qb/frontend/static/frontend/user_detail.js
webpack --mode development ./qb/frontend/src/quiz_detail.js --output ./qb/frontend/static/frontend/quiz_detail.js
webpack --mode development ./qb/frontend/src/quiz_take.js --output ./qb/frontend/static/frontend/quiz_take.js
webpack --mode development ./qb/frontend/src/populate_canvas.js --output ./qb/frontend/static/frontend/populate_canvas.js