from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('quiz/index', views.quiz_list, name="quizzes"),
    path('quiz/index/<int:page>/', views.quiz_list, name="quiz_index"),
    path('quiz/create', views.quiz_create, name="quiz_create"),
    path('quiz/<int:pk>/', views.quiz_detail, name="quiz_detail"),
    path('quiz/<int:pk>/take', views.quiz_take, name="quiz_take"),
    path('quiz/<int:pk>/edit', views.quiz_edit, name="quiz_edit"),

    path('grade/<int:pk>/', views.quiz_results, name="quiz_results"),

    path('user/<int:pk>/', views.user_detail, name="user_detail"),
]