import React from "react"
import RenderQuestions from "./ResultsComponents/RenderQestions"
import formatDate from "./helper_functions/formatDate"

class QuizResults extends React.Component {
    constructor(props){
        super(props)
        var answers_array = {}
        answers_array = JSON.parse(this.props.data.answers).answers
        console.log(answers_array)
        const answers_dict = {}
        answers_array.forEach(item => {
            answers_dict[item.id] = item.choice
        });

        this.state = {
            answers: answers_dict
        }

    }

    render () {
        console.log(this.props.data.quiz)
        const {quiz_name, author, questions, can_view_results, can_view_answers} = this.props.data.quiz
        const quiz_id = this.props.data.quiz.id
        const {created_at, id, score, total_score} = this.props.data

        let quiz_type_message;
        if(can_view_answers){
            quiz_type_message = "This quiz let's you view it's answer key."
        }else if(can_view_results){
            quiz_type_message = "This quiz let's you see your what you answered."
        }else {
            quiz_type_message = "This quiz only let's you see your grade."
        }

        return(
            <div>
                <h1>Results of <a href={"/quiz/" + quiz_id} className="quiz-title">{quiz_name}</a>, taken {formatDate(created_at)}</h1>
                <h3>Scored {score} out of {total_score}.</h3>
                <p>{quiz_type_message}</p>

                <br/>
                <RenderQuestions
                    questions={questions}
                    answers={this.state.answers}
                    can_view_results={can_view_results}
                    can_view_answers={can_view_answers}
                />
            </div>
        )
    }
}

export default QuizResults