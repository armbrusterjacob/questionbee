function OnEditSubmit (event, endpoint, csrf) {
    console.log("deleted to:", endpoint)
    fetch(endpoint,{
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRFToken': csrf
        },
    })
    // event.preventDefault();
}

export default OnEditSubmit