import React from "react"
import formatDate from "./helper_functions/formatDate"

function Grades () { return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Quiz Name</th>
                    <th>Published</th>
                    <th>Last Modified</th>
                </tr>
            </thead>
            <tbody>
                {data.quizzes.map((item, j) => {
                return (
                    <tr key={j}>
                        <td><a href={"/quiz/" + item.id.toString()}>{item.quiz_name}</a></td>
                        <td>{formatDate(item.created_at)}</td>
                        <td>{formatDate(item.last_modified)}</td>
                    </tr>
                ) 
                })}
            </tbody>
        </table>
    </div>
)}

class UserProfile extends React.Component {

    render () {
        console.log(this.props.data)
        const data = this.props.data;
        return (
            <div>
                <h1>{data.username}'s Profile</h1>
                <br/>
                <br/>
                <h2>Quizzes</h2>
                <br/>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Quiz Name</th>
                            <th>Published</th>
                            <th>Last Modified</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.quizzes.map((item, j) => {
                        return (
                            <tr key={j}>
                                <td><a href={"/quiz/" + item.id.toString()}>{item.quiz_name}</a></td>
                                <td>{formatDate(item.created_at)}</td>
                                <td>{formatDate(item.last_modified)}</td>
                            </tr>
                        ) 
                        })}
                    </tbody>
                </table>

            </div>
        )
    }
}

export default UserProfile