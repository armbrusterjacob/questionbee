import React from "react"
import Modal from "react-bootstrap/Modal"
import Button from "react-bootstrap/Button"

class DeleteButton extends React.Component{ 
    constructor(props){
        super(props)

        this.state ={
            show: false,
        }
        this.handleShow = this.handleShow.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    handleShow(){
        this.setState({show:true})
    }

    handleClose(){
        this.setState({show:false})
    }

    render(){
        return(
            <div>

            <button
                className="quiz-delete btn btn-danger float-right"
                type="button"
                onClick={this.handleShow}
            >
                Delete Quiz 
            </button>
            <Modal show={this.state.show} onHide={this.handleClose}>
                {/* <Modal.Header closeButton>
                    <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header> */}
                <Modal.Body className="modal-text">Are you sure you want to delete this quiz?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Close
                    </Button>
                    <Button variant="danger" 
                            onClick={() => this.props.handleDelete(event, this.props.endpoint, this.props.csrf)}>
                        Delete
                    </Button>
                </Modal.Footer>
            </Modal>

            </div>
        )
    }
}

export default DeleteButton