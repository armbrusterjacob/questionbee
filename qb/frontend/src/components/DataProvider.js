import React from "react"

class DataProvider extends React.Component {
    constructor() {
        super()
        this.state = {
            data: {},
            loaded: false,
            text: "Loading...",
        }
    }

    componentDidMount() {
        fetch(this.props.endpoint)
            .then(response => {
                if(response.status !== 200){
                    console.log("not loaded")
                    return this.setState({
                        text: "Couldn't retrieve data from backend...",
                    })
                    
                }
                return response.json();
            })
            .then(data => this.setState({
                data: data,
                loaded: true,
                text: "huh?",
            }))
    }

    render() {
        const {loaded, text, data} = this.state;
        return loaded ? this.props.render(data) : <p>{text}</p>
    }
}

export default DataProvider;