import React from "react"

const CAPITAL_ASCII_START = 65;

function RenderChoices (props) { 
    const {question, i} = props
    return (
    <div className="container">
    {question.choices.map((choice, j) => { return (
        <div key={j} className="row choice">
            <p className="choice-text">
                {String.fromCharCode(CAPITAL_ASCII_START + j)}) {choice.text}
            </p>
        </div>
    )})}
    </div>
)}

export default RenderChoices