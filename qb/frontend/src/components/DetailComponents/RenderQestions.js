import React from "react"
import RenderChoices from "./RenderChoices"
import QUESTION_TYPE from "../helper_functions/QuestionType"

function RenderQuestions(props) {
    const {questions, question_type} = props
    return(
    questions.map((question, i) => {return(
        <div className="question-detail container-fluid" key={i}>
            <div className="row">
                <h2 className="question-title">
                    {i + 1}. {question.text}   
                </h2>
            </div>

            {(() => {
                if(question.question_type == QUESTION_TYPE.MULTPLE_CHOICE ||
                    question.question_type == QUESTION_TYPE.CHOOSEALL
                ){
                    return(

                        <div>
                            <RenderChoices 
                                question={question} 
                                i={i}
                            /> 
                        </div>
                    )
                }
                return
            })()}
        </div>
    )})
)}

export default RenderQuestions