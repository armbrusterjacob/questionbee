import React from "react"
import QUESTION_TYPE from "./helper_functions/QuestionType"
import RenderQuestions from "./DetailComponents/RenderQestions";
import Button from "react-bootstrap/Button"

function TakeQuiz(takePath){ return(
    <a href={takePath}>
        <Button variant="primary">Take Quiz</Button>
    </a>
)}

function EditQuiz(user_id){ return(
    window.django.user.id == user_id ?
    <a href="edit"
       className="quiz-detail-edit float-right"
    >
     <Button variant="secondary">Edit Quiz</Button>
    </a> :
    ""
)}

class QuizDetail extends React.Component{
    render () {
        const data = this.props.data
        return (
            <div>
                <br/>
                <br/>
                {TakeQuiz(this.props.takePath)}
                {EditQuiz(data.owner)}
                <br/>
                <br/>
                <br/>
                <br/>
                {data.is_previewable ?
                    <RenderQuestions questions={data.questions}/> :
                    <h2>This quiz is not previewable.</h2>
                }
            </div>
        )
    }
}

export default QuizDetail;