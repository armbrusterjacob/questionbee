import React from "react"
import QUESTION_TYPE from "./helper_functions/QuestionType"
import { element } from "prop-types"

class Quiz extends React.Component {
    constructor(props) {
        super(props)
        const questions = this.props.data.questions
        this.state = {
            array: (questions.map((question) => {
                if(question.question_type == QUESTION_TYPE.CHOOSEALL){
                    return {
                        id: question.id,
                        choice: [],
                    }
                }
                return {
                    id: question.id,
                    choice: ""
                }
            }))
        }
        this.handleAnswer = this.handleAnswer.bind(this)
        LongText = LongText.bind(this)
        FillIn = FillIn.bind(this)
        MultipleChoice = MultipleChoice.bind(this)
        RadioChoice = RadioChoice.bind(this)
        CheckChoice = CheckChoice.bind(this)
    }


    handleAnswer(event, q_id){
        const {name, value, type, checked} = event.target
        this.setState((prevState) => {
            if(type === "checkbox")
            {
                if(checked){
                    prevState.array[q_id]['choice'].push(name.toString())
                }else{
                    prevState.array[q_id]['choice'] = prevState.array[q_id]['choice'].filter((element) => element.toString() != name.toString())
                }
            }else
            {
                prevState.array[q_id]['choice'] = value.toString()
            }
            
            return prevState
        })
    }

    render () {
        const questions = this.props.data.questions
        console.log(this.state)
        return (
            <form className="quiz-take-form">
                <br/>
                <br/>
                {questions.map((q, i) => {
                    switch(q.question_type){
                        case QUESTION_TYPE.MULTPLE_CHOICE:
                            return(MultipleChoice(q, RadioChoice, i))
                        case QUESTION_TYPE.FILLIN:
                            return(FillIn(q, i))
                        case QUESTION_TYPE.LONGTEXT:
                            return(LongText(q, i))
                        case QUESTION_TYPE.CHOOSEALL:
                            return(MultipleChoice(q, CheckChoice, i))
                        
                        default:
                            return(<p>[{q.text}] bad question type...</p>)
                }})}
                <button 
                    type="button" 
                    onClick={() => console.log(this.state)}
                    className="btn btn-secondary"
                > 
                    Display To Console
                </button>
                <br/>
                <br/>
                <button 
                    type="button" 
                    className="btn btn-save"
                    onClick={(event) => {
                        this.props.OnCreateSubmit(event, this.state.array, this.props.endpoint, this.props.redirect_base_url, this.props.csrf)
                    }}>
                    Save Quiz
                </button>
            </form>
        )
    }
};

function LongText(question, q_id) {
    const at = this.state.array[q_id]
    return (
        <div key={question.id}>    
            <h2 className="question-title">{question.text}</h2>
            <textarea 
                className="fillin"
                type="text"
                placeholder="[Put your essay response here]&#013;&#010;. . .&#013;&#010;. . ."
                name={question.id}
                value={at.choice}
                onChange={() => this.handleAnswer(event, q_id)}
            >
            </textarea>
            <br/>
            <br/>
            <br/>
        </div>
    )
}

function FillIn(question, q_id) {
    const at = this.state.array[q_id]
    return (
        <div key={question.id}>    
            <h2 className="question-title">{question.text}</h2>
            <input 
                className="quiz-take-choice"
                type="text" 
                placeholder="[Answer here]"
                name={question.id}
                value={at.choice}
                onChange={() => this.handleAnswer(event, q_id)}
            />
            <br/>
            <br/>
            <br/>
        </div>
    )
}

function MultipleChoice(question, type_funciton, q_id) {
    return (
        <div key={question.id}>    
            <h2 className="question-title">{question.text}</h2>
            {question.choices.map((c) => {
                return type_funciton(c, q_id)
            })}
            <br/>
            <br/>
            <br/>
        </div>
    )
}

function RadioChoice(choice, q_id) {
    const at = this.state.array[q_id]
    const is_checked = at.choice == choice.id
    return (
        <label key={choice.id} className={"custom-control custom-checkbox " + (is_checked && "custom-control-checked")}>
            <input className="custom-control-input"
                name={q_id} 
                type="radio" 
                value={choice.id || ""}
                checked={is_checked && "checked"}
                className="quiz-take-choice quiz-take-choice-radio"
                onChange={() => this.handleAnswer(event, q_id)}
            />
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">{choice.text}</span>
        </label>
    )
}


function CheckChoice(choice, q_id) {
    const at = this.state.array[q_id]
    const is_checked = !!at.choice.find((element) => element == choice.id)
    return (
        <label key={choice.id} className={"custom-control custom-checkbox " + (is_checked && "custom-control-checked")}>
            <input 
                name={choice.id || ""}
                type="checkbox" 
                className="quiz-take-choice quiz-take-choice-checkbox"
                checked={is_checked && "checked"}
                onChange={() => this.handleAnswer(event, q_id)}
            /> 
            <span className="custom-control-indicator"></span>
            <span className="custom-control-description">{choice.text}</span>
        </label>
    )     
}


export default Quiz;