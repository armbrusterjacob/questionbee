import React from "react"
import DrawCanvas from "./DrawCanvas"

function CanvasBar(props){ return (
    <div>
        <div className="card px-0 mx-0">
        <div className="card-img card-canvas">
                <DrawCanvas
                    name={props.quiz_name}
                    id={props.id}
                    />
            </div>
            <div className="card-img-overlay stick-bottom-container"
                style={{
                    width: "100%"
                    }}>
                <div className="stick-bottom px-0 py-0">
                    <div className="quiz-card-title">{props.quiz_name}</div>
                    <div className="mix-border" style={{marginBottom: "3px"}}/>
                </div> 
            </div>
        </div>
    </div>
)}

export default CanvasBar