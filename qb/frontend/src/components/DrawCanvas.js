import React from "react"
import seedrandom from "seedrandom"
import global_pallete, {COLOR_TYPE} from "./helper_functions/global_pallete"
import { basename } from "path";
class DrawCanvas extends React.Component{
    constructor(props){
        super(props);
        this.canvasRef = React.createRef();
        const id_string = this.props.id.toString()
        this.rng = seedrandom(id_string)
        const pallete_index = Math.floor(this.rng() * global_pallete.length)
        this.state = {
            pallete_index: pallete_index,
            height: 0,
            width: 0,
        }
        this.drawStripe = this.drawStripe.bind(this)
        this.randomColor = this.randomColor.bind(this)
        this.drawCanvas = this.drawCanvas.bind(this)
        this.drawBubbles = this.drawBubbles.bind(this)
        this.drawStripes = this.drawStripes.bind(this)
        this.drawTextCloud = this.drawTextCloud.bind(this)
        this.drawShapeGrid = this.drawShapeGrid.bind(this)
    }    

    changeTranspercy(color, newTransparency){
        const transparencyIndex = color.lastIndexOf(',') + 1
        color = color.substr(0, transparencyIndex)
        color += newTransparency.toString() + ")"
        return color
    }

    randomColor(type=COLOR_TYPE.ANY, customTransparecny=null)
    {
        const pallete = global_pallete[this.state.pallete_index]

        if(type == COLOR_TYPE.ANY)
            type = Math.floor(this.rng() * 2) ? COLOR_TYPE.BACKGROUND : COLOR_TYPE.FOREGROUND
        
        switch(type){
            case COLOR_TYPE.FOREGROUND:
                const colorIndex = Math.floor(this.rng() * pallete.fg_colors.length)
                if(customTransparecny)
                    return this.changeTranspercy(pallete.fg_colors[colorIndex], customTransparecny)
                return pallete.fg_colors[colorIndex]
            case COLOR_TYPE.BACKGROUND:
                const colorIndex2 = Math.floor(this.rng() * pallete.bg_colors.length)
                if(customTransparecny)
                    return this.changeTranspercy(pallete.bg_colors[colorIndex2], customTransparecny)
                return pallete.bg_colors[colorIndex2]
            default:
                // hot pink test color
                return "rgba(255,105,180,1)"
        }
    }
    
    drawStripe(position, degrees, thickness=10, color)
    {
        // add offset for when tan is inifnite
        if(degrees % 90 == 0)
            degrees += 0.0001
        
        // just invert stripe to avoid issues with degrees 180-360
        degrees %= 180

        const slope = Math.tan(degrees * Math.PI / 180)
        position.x -= thickness / 2
        position.y -= thickness / 2
        const canvas = this.canvasRef.current;
        const yIntercept = position.y - (slope * position.x)
        
        const innerRadians = ((90 - degrees) % 90) * Math.PI / 180
        const yDiff = Math.ceil(thickness / Math.sin(innerRadians))

        const yIntercept2 = yIntercept - yDiff
            
        const startingPoint = {x: 0, y: yIntercept}
        const startingPoint2 = {x: 0, y: yIntercept2}

        const y_max = (slope * canvas.width) + yIntercept

        const endPoint = {x: canvas.width, y: y_max}
        const endPoint2 = {x: canvas.width, y: y_max - yDiff}

        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.moveTo(startingPoint.x, startingPoint.y);
        ctx.lineTo(endPoint.x, endPoint.y);
        ctx.lineTo(endPoint2.x, endPoint2.y);
        ctx.lineTo(startingPoint2.x, startingPoint2.y);
        ctx.closePath();
        ctx.fillStyle = color;
        ctx.fill();
    }

    drawCircle(position, thickness=10, color){
        const canvas = this.canvasRef.current
        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.arc(position.x, position.y, thickness / 2, 2 * Math.PI, false)
        ctx.fillStyle = color;
        ctx.fill();
    }

    drawPalleteIndex(){
        var canvas = this.canvasRef.current
        var ctx = canvas.getContext("2d");
        ctx.font = "30px Arial";
        // hot pink test color
        ctx.fillStyle = "rgba(255,105,180,1)"
        ctx.fillText(this.state.pallete_index.toString() + " " + this.props.id, 20,  canvas.height - 4); 
    }

    componentDidMount() {
        this.setState({
            height: this.divElement.clientHeight,
            width: this.divElement.clientWidth,
        })
    }

     
    drawCanvas(){
        const canvas = this.canvasRef.current;
        const context = canvas.getContext('2d');

        context.fillStyle = this.randomColor(COLOR_TYPE.BACKGROUND);
        context.fillRect(0, 0, canvas.width, canvas.height);
        
        var f_list = [this.drawStripes, this.drawBubbles, this.drawShapeGrid];
        // only use the text cloud style if a certain length is satisfied
        if(this.props.name.replace(/\s/g, '').length >= 6)
            f_list.push(this.drawTextCloud)

        f_list[Math.floor(this.rng() * f_list.length)]();

        // // draw pallet index for testing purposes
        // this.drawPalleteIndex()
    }
    
    drawStripes(){
        const canvas = this.canvasRef.current

        const thicknessBase = 70
        const thicknessOffset = 100
        const count = 1 + Math.floor(this.rng() * 3)
        const angleBase = 45
        const angleOffset = 10
        const angleInverse = this.rng() > 0.5 ? -1 : 1
        const angle = angleInverse * (angleBase + (this.rng() * angleOffset))

        const borderOffset = 15;

        // var positions = []
        for (let i = 0; i < count; i++) {
            const thickness = thicknessBase + (this.rng() * thicknessOffset)
            const borderThicknessOffset = borderOffset + thickness
            const x = borderThicknessOffset + (this.rng() * (canvas.width -  (borderThicknessOffset * 2)))
            const y = borderThicknessOffset + (this.rng() * (canvas.height -  (borderThicknessOffset * 2)))
            const newPos =  {x: x, y: y}
            this.drawStripe(newPos, angle, thickness, this.randomColor(COLOR_TYPE.FOREGROUND))
            // positions.push(newPos)
        }
    }
    
    drawBubbles(){
        // draw bubbles that get progressivley smaller until a minimum size
        const canvas = this.canvasRef.current
        const thicknessBase = 160
        const thicknessOffset = 45
        const thicknessMinimum = 40
        const reduceBase = 30
        const reduceOffset = 30
        const borderOffset = 15;
        const color = this.randomColor(COLOR_TYPE.FOREGROUND)

        var thickness = thicknessBase + (this.rng() * thicknessOffset)
        while(thickness > thicknessMinimum){
            const borderThicknessOffset = borderOffset
            const x = borderThicknessOffset + (this.rng() * (canvas.width -  (borderThicknessOffset * 2)))
            const y = borderThicknessOffset + (this.rng() * (canvas.height -  (borderThicknessOffset * 2)))
            const newPos =  {x: x, y: y}
            this.drawCircle(newPos, thickness, color)
            thickness -= (reduceBase + (this.rng() * reduceOffset))
        }
        
    }

    drawTextCloud(){
        // draw bubbles that get progressivley smaller until a minimum size
        const canvas = this.canvasRef.current
        var ctx = canvas.getContext('2d');
        const sizeBase = 100
        const sizeOffset = 50
        const borderOffset = 15;
        const color = this.randomColor(COLOR_TYPE.FOREGROUND, 0.2)
        const fontList = [
            "Arial",
            // "Roboto",
            "Times",
            // "Courier",
            "Verdana",
            // "Impact",
        ]
        const font = fontList[Math.floor(this.rng() * fontList.length)]
        ctx.fillStyle = color
        
        const name = this.props.name
        for (let i = 0; i < name.length; i++) {
            const size = sizeBase + (sizeOffset * this.rng())
            ctx.font = size + "px " + font;
            const borderSizeOffset = borderOffset
            const x = borderSizeOffset + (this.rng() * (canvas.width -  borderSizeOffset))
            const y = borderSizeOffset + (this.rng() * (canvas.height -  borderSizeOffset))
            ctx.fillText(name[i], x, y); 
        }
    }

    drawShapeGrid(){
        const canvas = this.canvasRef.current
        var ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        const cellSize = 110
        const cellOffsetX = this.rng() * cellSize
        const cellOffsetY = this.rng() * cellSize

        // includes partial cells on left/top and right/bottom due to cellOffset
        const horizontalCellCount = Math.ceil((canvas.width - cellOffsetX) / cellSize) + (cellOffsetX == 0 ? 0 : 2)
        const verticalCellCount = Math.ceil((canvas.height - cellOffsetX) / cellSize) + (cellOffsetY == 0 ? 0 : 2)

        const startX = cellOffsetX - cellSize
        const startY = cellOffsetY - cellSize

        function draw(){
            ctx.fillStyle = color
            for (let i = 0; i < horizontalCellCount ; i++) {
                const x = startX + (cellSize * i)
                for (let j = 0; j < verticalCellCount ; j++) {
                    const y = startY + (cellSize * j)
                    const xRandomlyOffset = x + (img.width / 2) + this.rng() * (cellSize - img.width)
                    const yRandomlyOffset = y + (img.height / 2) + this.rng() * (cellSize - img.height)

                    // draw image centered around rotation pivot
                    ctx.save();
                    ctx.translate(xRandomlyOffset, yRandomlyOffset);
                    ctx.rotate(this.rng() * Math.PI * 2);
                    ctx.drawImage(img,-img.width / 2, -img.height / 2);
                    ctx.restore()
                }
            }
            
            // Fill images with foreground color
            ctx.globalCompositeOperation = "source-in"
            ctx.fillStyle = color
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            
            // Create background under those filled images
            ctx.globalCompositeOperation = "destination-over"
            ctx.fillStyle = bgColor
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            
            // reset 
            ctx.globalCompositeOperation = "source-over";
        }
        draw = draw.bind(this)

        const imageSet = [
            {url: "/static/images/shapes/square.png", transparency: .5},
            {url: "/static/images/shapes/circle.png", transparency: .7},
            {url: "/static/images/shapes/triangle.png", transparency: .5},
            {url: "/static/images/shapes/star.png", transparency: .7},
            {url: "/static/images/shapes/4star.png", transparency: 1},
            {url: "/static/images/shapes/roundSquare.png", transparency: 1},
            {url: "/static/images/shapes/openSquare.png", transparency: 0.5},
            {url: "/static/images/shapes/openTriangle.png", transparency: 0.5},
            {url: "/static/images/shapes/openCircle.png", transparency: 0.5},
        ]

        const imgIndex = Math.floor(this.rng() * imageSet.length)

        const bgColor = this.randomColor(COLOR_TYPE.BACKGROUND)
        const color = this.randomColor(COLOR_TYPE.FOREGROUND, imageSet[imgIndex].transparency)

        var img = new Image()
        img.src =  imageSet[imgIndex].url
        img.onload = draw
    }

    componentDidUpdate(){
        this.drawCanvas()
    }

    render(){
        return(
            <div className="card-background px-0" 
                 ref={ (divElement) => this.divElement = divElement}
            >
                <canvas ref={this.canvasRef} width={this.state.width} height={this.state.height}>
                    {/* <div className="card-background"></div> */}
                </canvas>
            </div>
        )
    }
}

export default DrawCanvas;