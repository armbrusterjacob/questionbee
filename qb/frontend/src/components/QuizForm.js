import React from "react"
import QUESTION_TYPE from "./helper_functions/QuestionType"
import RenderQuestions from "./FormComponents/RenderQestions";
import Tooltip from "./FormComponents/ToolTip"

class QuizForm extends React.Component{
    constructor(){
        super()
        this.state = {
            error_messages: [],
            quiz_name: "",
            questions: [],
            is_private: false,
            is_previewable: true,
            can_view_results: true,
            can_view_answers: false,
            submit_disabled: false,
        };

        this.addQuestion = this.addQuestion.bind(this)
        this.removeQuestion = this.removeQuestion.bind(this)
        this.swapQuestion = this.swapQuestion.bind(this)
        this.addChoice = this.addChoice.bind(this)
        this.removeChoice = this.removeChoice.bind(this)
        this.swapChoice = this.swapChoice.bind(this)
        this.handleQuizChange = this.handleQuizChange.bind(this)
        this.handleQuestionChange = this.handleQuestionChange.bind(this)
        this.handleChoiceChange = this.handleChoiceChange.bind(this)
        this.toggle_submit = this.toggle_submit.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.validtate = this.validate.bind(this)
    }

    toggle_submit() {
        this.setState((prevState) => {
            return({
                submit_disabled: !prevState.submit_disabled
            })
        })
    }

    validate() {
        const {quiz_name, questions} = this.state
        var error_messages = []
        if(quiz_name.replace(/\s/g, '').length == 0){
            error_messages.push("Names for quizzes are required.")
        }
        questions.forEach((question, i) => {
            if(question.text.replace(/\s/g, '').length == 0){
                error_messages.push("Question #" + (i+1).toString()  +" needs a name.")
            }else{
                if(question.question_type != QUESTION_TYPE.LONGTEXT)
                {
                    question.choices.some((choice) =>{
                        if(choice.text.replace(/\s/g, '').length == 0){
                            error_messages.push("Question #" + (i+1).toString() + " has a choice without text.")
                            return true;
                        }
                    })
                }
            }
            
            if(question.choices.length == 0 &&
                question.question_type != QUESTION_TYPE.LONGTEXT){
                    error_messages.push("Question #" + (i+1).toString() + " has no choices.")
                    if(question.choices.every((choice) => !choice.is_correct)){
                        error_messages.push("Question #" + (i+1).toString() + " has no correct choice.")
                    }
            }
         })
            
        this.setState((prevState) => {
            return({
                error_messages: error_messages
            })
        })

        return error_messages.length == 0
    }

    async handleSubmit() {
        this.toggle_submit()

        try{
            if(this.validate()){
                await this.props.onSubmit(event, this.state, this.props.endpoint, this.props.redirect_base_url,  this.props.csrf)
            }
        }
        catch(error){
            console.error(error);
            const error_messages = ['Quiz was not successfully created due to a database error.']
            this.setState((prevState) => {
                return({
                    error_messages: error_messages
                })
            })
        }
        finally{
            this.toggle_submit()
        }
    }

    componentDidMount() {
        if(this.props.data)
        {
            this.setState(this.props.data)
        }else{
            this.addQuestion()
        }
        this.forceUpdate();
    }

    addQuestion() {
        this.setState((prevState) => {
            prevState.questions.push({
                text: "",
                question_type: QUESTION_TYPE.MULTPLE_CHOICE,
                choices: [
                    {
                        text: "",
                        is_correct: true,
                    }
                ]
            })
            return {
                questions: prevState.questions
            }
        })
    }

    removeQuestion(index) {
        this.setState((prevState) => {
            console.log(index)
            prevState.questions.splice(index, 1)
            return {
                questions: prevState.questions
            }
        })
    }

    swapQuestion(index1, index2) {
        this.setState((prevState) => {
            const q =  prevState.questions
            const temp = q[index1]
            q[index1] = q[index2]
            q[index2] = q[temp]
            return {
                questions: q
            }
        })
    }

    addChoice(q_index) {
        this.setState((prevState) => {
            prevState.questions[q_index].choices.push({
                text: "",
                is_correct: false,
            })
            return {
                questions: prevState.questions
            }
        })
    }

    removeChoice(q_index, c_index) {
        this.setState((prevState) => {
            prevState.questions[q_index].choices.splice(c_index, 1)
            return {
                questions: prevState.questions
            }
        })
    }

    swapChoice(q_index, c_index1, c_index2) {
        this.setState((prevState) => {
            const q =  prevState.questions[q_index].choices
            const temp = q[c_index1]
            q[c_index1] = q[c_index2]
            q[c_index2] = q[temp]
            return {
                questions: prevState.questions
            }
        })
    }

    handleQuizChange(event){
        const {name, value, type, checked} = event.target
        if(type === "checkbox")
        {
            this.setState({
                [name]: checked
            })
        }
        else
        {
            this.setState({
                [name]: value
            })
        }
    }

    handleQuestionChange(q_index, event){
        const {name, value, type, checked} = event.target
        this.setState((prevState) => {
            if(type === "checkbox")
            {
                prevState.questions[q_index][name] = checked
            }else
            {
                prevState.questions[q_index][name] = value
            }
            return({
                questions: prevState.questions
            })
        })
    }

    handleChoiceChange(q_index, c_index, event){
        const {name, value, type, checked} = event.target

        this.setState((prevState) => {
            if(type === "checkbox")
            {
                prevState.questions[q_index].choices[c_index]["is_correct"] = checked
            }else if(type === "radio")
            {
                prevState.questions[q_index].choices.forEach((element) => {
                    element["is_correct"] = false
                })
                
                prevState.questions[q_index].choices[c_index]["is_correct"] = checked
                // console.log(prevState.questions[q_index].choices[c_index]["is_correct"]);
            }else
            {
                prevState.questions[q_index].choices[c_index][name] = value
            }
            return({
                questions: prevState.questions
            })
        })

    }
    render () {
        const {questions, error_messages} = this.state
        return (
            <div>
            {error_messages.map((message, i) => { return(
                <div id="alert-box" key={i}>
                    <div className="alert alert-danger" role="alert">
                        Error: {message}
                    </div>
                </div>
            )})}

            <form>
                <input type="hidden" name="csrfmiddlewaretoken" value={this.props.csrf} />
                <input
                    className="quiz-title-input"
                    type="text"
                    name="quiz_name"
                    placeholder="Quiz Title"
                    value={this.state.quiz_name}
                    onChange={(event) => this.handleQuizChange(event)}
                />
                <br/>
                <br/>


                <Tooltip text="If set to private quizzes won't show in on the main quiz list page."/>
                <label>Private?</label>
                <input
                    className="quiz-is-private"
                    type="checkbox"
                    name="is_private"
                    checked={this.state.is_private ? "checked" : ""}
                    onChange={(event) => this.handleQuizChange(event)}
                    />

                <br/>
        
                <Tooltip text="When set you can look at the quiz's questions before taking it."/>
                <label>Previewable? </label>
                <input
                    className="quiz-is-previewable"
                    type="checkbox"
                    name="is_previewable"
                    checked={this.state.is_previewable ? "checked" : ""}
                    onChange={(event) => this.handleQuizChange(event)}
                />
                <br/>

                <Tooltip text="If set, people who take this quiz can view their answers afterwards."/>
                <label>Results viewable? </label>
                <input
                    className="quiz-can-view-results"
                    type="checkbox"
                    name="can_view_results"
                    checked={this.state.can_view_results ? "checked" : ""}
                    onChange={(event) => this.handleQuizChange(event)}
                />
                <br/>

                <Tooltip text="Let's those who take your quiz see the answers with their feedback."/>
                <label>Show Answers in results? </label>
                <input
                    className={"quiz-can-view-answers" + (this.state.can_view_results && "disabled")}
                    type="checkbox"
                    name="can_view_answers"
                    disabled={!this.state.can_view_results ? "checked" : ""}
                    checked={this.state.can_view_answers && this.state.can_view_results ? "checked" : ""}
                    onChange={(event) => this.handleQuizChange(event)}
                />
                <br/>

                <br/>
                <hr/>
                
                <RenderQuestions 
                    questions={questions}
                    handleQuestionChange={this.handleQuestionChange}
                    removeQuestion={this.removeQuestion}
                    addChoice={this.addChoice}
                    choice_functions={{
                        handleChoiceChange: this.handleChoiceChange,
                        removeChoice: this.removeChoice,
                    }}
                />
                <button 
                    type="button"
                    className="btn btn-secondary"
                    onClick={this.addQuestion}>
                    Add Question
                </button>
                <br/>
                <br/>
                <br/>
                <button 
                    type="button"
                    className="btn btn-secondary"
                    onClick={() => console.log(this.state)}>
                    Display to console</button>
                <br/>
                <br/>
                <button 
                    type="button" 
                    className="btn btn-save"
                    disabled={this.state.submit_disabled}
                    onClick={this.handleSubmit}>
                    Save Quiz
                </button>
            </form>
            </div>
        )
    }
}

export default QuizForm;