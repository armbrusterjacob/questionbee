function OnEditSubmit (event, data, endpoint, redirect_url, csrf) {
    console.log("sent to:", endpoint)
    event.preventDefault();
    fetch(endpoint,{
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'X-CSRFToken': csrf
        },
        body: JSON.stringify(data)
    })
    .then((response) => {
        if(response.status !== 201) // http create
            return Promise.reject(new Error("Quiz was not successfully created"))
        else 
            return response.json();
    })
    .then((response) => {
        console.log(response)
        if(response)
            window.location.href = redirect_url + response.id.toString();
        else
            console.log("Failed to create quiz")
    })
    .catch((error) => {
        console.log(error.message)
    })
    .finally(() => {
        return true
    })

    return false
}

export default OnEditSubmit
