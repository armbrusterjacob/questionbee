export let COLOR_TYPE = {
    ANY  : 0,
    BACKGROUND  : 1,
    FOREGROUND: 2,
};

const global_pallete = [
    {
        // 0 reddish
        bg_colors: [
            'rgba(85,83,97,1)',
            'rgba(82,76,65,1)',
        ],
        fg_colors:[
            'rgba(142,87,93,1)',
            'rgba(223,128,100,1)',
            'rgba(232,208,160,1)',
            
        ]
    },
    {
        // 1 purpleish
        bg_colors: [
            'rgba(41,17,26,1)',
            'rgba(28,14,29,1)',
        ],
        fg_colors:[
            'rgba(94,78,105,1)',
            'rgba(113,140,168,1)',
            
        ]
    },
    {
        // 2 pink and blue
        bg_colors:[
            "rgba(21,1,26,1)",
        ],
        fg_colors:[
            "rgba(20,50,97,1)",
            "rgba(14,148,147,1)",
            "rgba(252,184,168,1)",
        ]
    },
    {
        // 3 dark and secondaries
        bg_colors:[
            "rgba(27,15,1,1)",
            
        ],
        fg_colors:[
            "rgba(60,173,85,1)",
            "rgba(2,138,124,1)",
            "rgba(218,189,87,1)",
        ]
    },
    {
        //pink and blue
        bg_colors:[
            "rgba(68,47,46,1)",
            "rgba(168,155,136,1)",
            
        ],
        fg_colors:[
            "rgba(195,20,53,1)",
            "rgba(86,112,49,1)",
        ]
    }
];
export default global_pallete