function pluralize(str, num)
{
    console.log(num, num > 1)
    return str + (num > 1 ? "s" : "")
}

const MILLISECOND_CONV = {
    SECONDS  : (1000),
    MINUTES  : (1000 * 60),
    HOURS  : (1000 * 60 * 60),
    DAYS  : (1000 * 60 * 60 * 24),
    MONTHS  : (1000 * 60 * 60 * 24 * 31),
    YEARS  : (1000 * 60 * 60 * 24 * 365),
}

function format(word, num){
    if(word == "second" && num < 30)
        return "just now"
    else
        return num + " " + pluralize(word, num) + " ago"
}

function formatDate(timestamp){
    const now = new Date();
    const date = new Date(timestamp);

    const difference = (now - date);
    

    if(difference < MILLISECOND_CONV.MINUTES){
        const num = Math.floor(difference / MILLISECOND_CONV.SECONDS)
        return format("second", num)
    }
    if(difference < MILLISECOND_CONV.HOURS){
        const num = Math.floor(difference / MILLISECOND_CONV.MINUTES)
        return format("minute", num)
    }
    if(difference < MILLISECOND_CONV.DAYS){
        const num = Math.floor(difference / MILLISECOND_CONV.HOURS)
        return format("hour", num)
    }
    if(difference < MILLISECOND_CONV.MONTHS){
        const num = Math.floor(difference / MILLISECOND_CONV.DAYS)
        return format("day", num)
    }
    if(difference < MILLISECOND_CONV.YEARS){
        const num = Math.floor(difference / MILLISECOND_CONV.MONTHS)
        return format("month", num)
    }
    const num = Math.floor(difference / MILLISECOND_CONV.YEARS)
    return format("year", num)
}

export default formatDate;