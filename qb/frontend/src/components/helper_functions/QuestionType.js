const QUESTION_TYPE = {
    MULTPLE_CHOICE  : "MC",
    FILLIN  : "FI",
    LONGTEXT  : "LT",
    CHOOSEALL  : "CA",
}

export default QUESTION_TYPE;