import Cookies from  "js-cookie"

function GetCSRFToken () {
    return Cookies.get('csrftoken')
}

export default GetCSRFToken;