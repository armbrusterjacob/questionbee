function getURLDetails(url=window.location.pathname){
    const words = url.split('/', 3)
    
    if(words.length < 2)
        throw "URL has less than 2 parameters...";

    return {
        model: words[1],
        pk: words[2]
    }
}

export default getURLDetails