import React from "react"
import RenderChoices from "./RenderChoices"
import QUESTION_TYPE from "../helper_functions/QuestionType"
import QuizResults from "../QuizResults";

const CAPITAL_ASCII_START = 65;

function RenderQuestions(props) {
    const {questions, question_type, answers} = props
    return(
    questions.map((question, i) => {return(
        <div className="question-detail container-fluid" key={i}>
            <div className="row">
                <h2 className="question-title">
                    {i + 1}. {question.text}   
                </h2>
            </div>

            {(() => {

                if(question.question_type == QUESTION_TYPE.MULTPLE_CHOICE ||
                    question.question_type == QUESTION_TYPE.CHOOSEALL
                ){
                    // Find which answer(s) were chosen
                    var chosen = []
                    if(question.question_type == QUESTION_TYPE.MULTPLE_CHOICE)
                        chosen.push(answers[question.id])
                    if(question.question_type == QUESTION_TYPE.CHOOSEALL)
                    {
                        chosen = answers[question.id]
                    }
                    return(

                        <div>
                            <p>You chose: {
                                question.choices.map((choice, j) => {
                                    if(!chosen.includes(choice.id.toString()))
                                        return ""
                                    return String.fromCharCode(CAPITAL_ASCII_START + j)
                                }).filter((item) => {
                                    return item != "" && item != null;
                                }).join(',')
                            }
                            </p>
                            <RenderChoices 
                                question={question} 
                                i={i}
                                chosen={chosen}
                            /> 
                        </div>
                    )
                }else
                {
                    let buffer;
                    if(question.choices.length == 1)
                        buffer = "Correct answer was \"" + question.choices[0].text + "\""
                    else{
                        const correct_choices = question.choices.filter((element) => {
                            return !!element.is_correct
                        })
                        .map((element) =>{
                            return "\"" + element.text  + "\""
                        })
                        const last = correct_choices.pop()

                        buffer = "Corect answers were " + correct_choices.join(", ") + ", and " + last
                    }
                    return(
                        <div>
                            <p>
                                You answered <mark>'{answers[question.id]}'</mark>.
                            </p>
                            <p>{buffer}</p>
                        </div>
                    )
                }
            })()}
        </div>
    )})
)}

export default RenderQuestions