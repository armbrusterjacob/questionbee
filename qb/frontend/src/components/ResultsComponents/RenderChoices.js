import React from "react"

const CAPITAL_ASCII_START = 65;

function RenderChoices (props) { 
    const {question, i, chosen} = props
    return (
    <div className="container">
    {question.choices.map((choice, j) => { return (
        <div key={j} className="row choice">
            {(() => {
                let buffer;
                const was_chosen = chosen.includes(choice.id.toString())
                const image_size = 32;
                if(typeof(choice.is_correct) != "undefined"){
                    if(was_chosen){
                        buffer = <img 
                            src={"/static/images/" + (choice.is_correct ==  was_chosen ? "correct.png" : "incorrect.png")}
                            alt={choice.is_correct ==  was_chosen ? "[correct] " : "[incorrect] "}
                            width={image_size} 
                            height={image_size}
                        />
                    }else{
                        buffer = <span style={{marginLeft: image_size + 'px'}}/>
                    }
                }else{
                    buffer = ""
                }

                // Highlight the chosen choice(es)
                if(was_chosen) 
                    return(
                        <p className="choice-text"> 
                            {buffer}
                            <mark>
                                {String.fromCharCode(CAPITAL_ASCII_START + j)}) {choice.text}
                            </mark>
                        </p>

                    )
                else
                    return(
                        <p className="choice-text">
                            {buffer}
                            {String.fromCharCode(CAPITAL_ASCII_START + j)}) {choice.text}
                        </p>
                    )
            })()}
        </div>
    )})}
    </div>
)}

export default RenderChoices