import React from "react"
import formatDate from "./helper_functions/formatDate"

class QuizTable extends React.Component {

    render () {
        console.log(this.props.data)
        const data = this.props.data;
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Quiz Name</th>
                            <th>Published</th>
                            <th>Author</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((item, j) => {
                        return (
                            <tr key={j}>
                                <td><a href={"/quiz/" + item.id.toString()}>{item.quiz_name}</a></td>
                                <td>{formatDate(item.created_at)}</td>
                                <td><a href={"/user/" + item.owner.toString()}>{item.author}</a></td>
                            </tr>
                        ) 
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default QuizTable