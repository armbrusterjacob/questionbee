import React from "react"
// import ButtonToolbar from 'react-tooltip'
import {Button, OverlayTrigger, Tooltip} from "react-bootstrap"


function ToolTip(props) { return(
        <OverlayTrigger
            overlay={
                <Tooltip id="tooltip-top">
                    {props.text}
                </Tooltip>
            }
            >
            <Button
                className="delete-question py-0 px-1 btn btn-secondary ml-auto col-auto"
            >
                [?]
            </Button>
        </OverlayTrigger>
)}

export default ToolTip;