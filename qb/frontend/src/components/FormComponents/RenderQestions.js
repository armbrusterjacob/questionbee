import React from "react"
import RenderChoices from "./RenderChoices"
import QUESTION_TYPE from "../helper_functions/QuestionType"

function RenderQuestions(props) {
    const {questions} = props
    return(
    questions.map((question, i) => {return(
        <div className="question question-edit container-fluid" key={i}>
        <div className="row">

        <select 
            name="question_type"
            value={question.question_type}
            className="question-type"
            onChange={(event) => props.handleQuestionChange(i, event)}
            >
            <option value="">Choose a Question Type</option>
            <option value={QUESTION_TYPE.MULTPLE_CHOICE}>Multiple Choice</option>
            <option value={QUESTION_TYPE.FILLIN}>Fill in</option>
            <option value={QUESTION_TYPE.LONGTEXT}>Essay</option>
            <option value={QUESTION_TYPE.CHOOSEALL}>Choose all</option>
        </select>

        {questions.length > 1 && 
            <button 
            type="button" 
            onClick={() => props.removeQuestion(i)}
            className="delete-question py-0 btn btn-secondary ml-auto col-auto">
            X</button>
        }

        </div>

        <div className="row">

        <input type="text"
            className="question-text"
            name="text"
            placeholder="Question Text"
            value={question.text}
            onChange={(event) => props.handleQuestionChange(i, event)}
            />
        </div>

        {(() => {
            if(question.question_type != QUESTION_TYPE.LONGTEXT)
                return(
                    <div>
                        <RenderChoices 
                        question={question} 
                        i={i}
                        choice_functions={props.choice_functions}
                        />
                        <br/>
                        <button 
                            onClick={() => props.addChoice(i)} type="button"
                            className="btn btn-add">
                                Add Choice
                        </button>
                    </div>
                )
            return
        })()}
    </div>
    )})
)}

export default RenderQuestions