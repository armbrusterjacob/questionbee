import React from "react"
import QUESTION_TYPE from "../helper_functions/QuestionType"

function RenderChoices (props) { 
    const {question, i, choice_functions} = props
    return (
    <div className="container">
    {/* for each choice in each question */}
    {question.choices.map((choice, j) => { return (
        <div key={j} className="row choice">
            <div className="col-3 input-group  mt-1">
                <div className="input-group-prepend">
                    <div className="input-group-text">
                        <input type={question.question_type == QUESTION_TYPE.MULTPLE_CHOICE ? "radio" : "checkbox"}
                            name={"is_correct-" + i + "-" + j}
                            className="choice-is-correct"
                            checked={choice.is_correct || question.question_type == QUESTION_TYPE.FILLIN}
                            disabled={question.question_type == QUESTION_TYPE.FILLIN && "checked"}
                            onChange={(event) => choice_functions.handleChoiceChange(i,j, event)}
                            />
                    </div>
                </div>
                <input type="text" 
                    name="text"
                    value={choice.text}
                    className="choice-input"
                    placeholder="Choice Text"
                    onChange={(event) => choice_functions.handleChoiceChange(i,j, event)}
                />
            </div>
            <div >
            {
                question.choices.length > 1 && 
                <button 
                className="delete-choice py-0 btn btn-secondary btn-sm"
                type="button" 
                onClick={() => choice_functions.removeChoice(i, j)}>
                X</button>
            }
            </div>
        </div>
    )})}
    </div>
)}

export default RenderChoices