import React from "react"
import ReactDOM from "react-dom"
import QuizForm from "./components/QuizForm"
import GetCSRFToken from "./components/helper_functions/GetCSRFToken"
import OnCreateSubmit from "./components/OnCreateSubmit"

ReactDOM.render(
    <QuizForm 
        endpoint="/api/quiz/" 
        redirect_base_url="/quiz/"
        csrf={GetCSRFToken()}
        onSubmit={OnCreateSubmit}
    />, 
    document.getElementById("root")
)