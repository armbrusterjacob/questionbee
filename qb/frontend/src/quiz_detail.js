import React from "react"
import ReactDOM from "react-dom"
import getURLDetails from "./components/helper_functions/getURLDetails"
import DataProvider from "./components/DataProvider"
import QuizDetail from "./components/QuizDetail"
import CanvasBar from "./components/CanvasBar"

const pathDetails = getURLDetails()
const path = "/api/" + pathDetails.model + "/" + pathDetails.pk + "/"
const takePath = "/" + pathDetails.model + "/" + pathDetails.pk + "/take"

console.log(path)

ReactDOM.render(            
    <DataProvider 
        endpoint={path}
        render={data =>
            <div>
                <CanvasBar quiz_name={data.quiz_name} id={data.id}/>
                <QuizDetail data={data} takePath={takePath} />
            </div>
        }
    />,
    document.getElementById("root")
)