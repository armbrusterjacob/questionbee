import React from "react"
import ReactDOM from "react-dom"
import QuizForm from "./components/QuizForm"
import DataProvider from "./components/DataProvider"
import DeleteButton from "./components/DeleteButton"
import OnDeleteSubmit from "./components/OnDeleteSubmit"
import getURLDetails from "./components/helper_functions/getURLDetails"
import GetCSRFToken from "./components/helper_functions/GetCSRFToken"
import OnEditSubmit from "./components/OnEditSubmit"

const pathDetails = getURLDetails()
const path = "/api/" + pathDetails.model + "/" + pathDetails.pk + "/"

console.log(path)

ReactDOM.render(<DataProvider 
    endpoint={path}
    render={data => { return (
        <div>
            <DeleteButton
                endpoint={path}
                csrf={GetCSRFToken()}
                handleDelete={OnDeleteSubmit}
            />
            <QuizForm 
            endpoint={path} 
            redirect_base_url="/quiz/"
            csrf={GetCSRFToken()}
            onSubmit={OnEditSubmit}
            data={data}
            />
        </div>
        )}}
    />,
    document.getElementById("root")
)
