import React from "react"
import ReactDOM from "react-dom"
import DrawCanvas from "./components/DrawCanvas"

var elements = document.getElementsByClassName("card-img")
for(var i = 0; i <elements.length; i++){
    ReactDOM.render(
        <DrawCanvas
            name={elements[i].getAttribute("name")}
            id={elements[i].getAttribute("id")}
        />,
        elements[i])
}