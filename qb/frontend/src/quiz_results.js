import React from "react"
import ReactDOM from "react-dom"
import getURLDetails from "./components/helper_functions/getURLDetails"
import DataProvider from "./components/DataProvider"
import QuizResults from "./components/QuizResults"

const pathDetails = getURLDetails()
const path = "/api/" + pathDetails.model + "/" + pathDetails.pk + "/"

console.log(path)

ReactDOM.render(            
    <DataProvider 
        endpoint={path}
        render={data => <QuizResults data={data} />}
    />,
    document.getElementById("root")
)