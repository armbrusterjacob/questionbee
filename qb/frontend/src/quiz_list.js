import React from "react"
import ReactDOM from "react-dom"
import DataProvider from "./components/DataProvider"
import QuizTable from "./components/QuizTable"

ReactDOM.render(<DataProvider 
                    endpoint="/api/quiz"
                    render={data => <QuizTable data={data} />}
                />,
                document.getElementById("root")
)