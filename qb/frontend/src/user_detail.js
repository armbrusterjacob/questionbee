import React from "react"
import ReactDOM from "react-dom"
import getURLDetails from "./components/helper_functions/getURLDetails"
import DataProvider from "./components/DataProvider"
import UserProfile from "./components/UserProfile"

const pathDetails = getURLDetails()
const path = "/api/" + pathDetails.model + "/" + pathDetails.pk + "/"

console.log(path)


ReactDOM.render(<DataProvider 
    endpoint={path}
    render={data => { return (
        <UserProfile
            data={data}
        />
        )}}
    />,
    document.getElementById("root")
)