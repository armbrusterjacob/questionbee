import React from "react"
import ReactDOM from "react-dom"
import getURLDetails from "./components/helper_functions/getURLDetails"
import GetCSRFToken from "./components/helper_functions/GetCSRFToken"
import OnCreateSubmit from "./components/OnCreateSubmit"
import DataProvider from "./components/DataProvider"
import Quiz from "./components/Quiz"
import CanvasBar from "./components/CanvasBar"

const pathDetails = getURLDetails()
const path = "/api/" + pathDetails.model + "/" + pathDetails.pk + "/"

console.log(path)

ReactDOM.render(            
    <DataProvider 
        endpoint={path}
        render={data => 
            <div>
                <CanvasBar quiz_name={data.quiz_name} id={data.id}/>
                <Quiz data={data} 
                    OnCreateSubmit={OnCreateSubmit}
                    redirect_base_url="/grade/"
                    endpoint={"/api/grade/" + pathDetails.pk + "/"}
                    csrf={GetCSRFToken()}
                />
            </div>
        }
    />,
    document.getElementById("root")
)