from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.models import User
from quiz.models import Quiz, Grade
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

def index(request):
    return render(request, 'frontend/index.html')

def quiz_list(request, page=1):
    if(page < 1):
        page = 1

    quizzes = Quiz.objects.all().filter(is_private=False).order_by('-created_at')
    paginator = Paginator(quizzes, 15)
    quiz_data = paginator.page(page)

    return render(request, 'frontend/quiz_list.html', {"quiz_data": quiz_data})

@login_required
def user_detail(request, pk):
    try:
        user = User.objects.get(pk=pk)
    except User.DoesNotExist:
        raise Http404("User does not exist.")

    show_grades = request.user.id == user.id and user.grades.exists()
    return render(request, 'frontend/user_detail.html', {"user_data": user, "show_grades": show_grades})

# Views that use React
@login_required
def quiz_results(request, pk):
    try:
        grade = Grade.objects.get(pk=pk)
    except Grade.DoesNotExist:
        raise Http404("Quiz does not exist.")



    if(request.user.id != grade.user.id and not request.user.is_staff):
        raise Http404("Not authroized to edit other users quizzes.")

    return render(request, 'frontend/quiz_results.html')

@login_required
def quiz_edit(request, pk):
    try:
        quiz = Quiz.objects.get(pk=pk)
    except Quiz.DoesNotExist:
        raise Http404("Quiz does not exist.")

    if(request.user.id != quiz.owner.id and not request.user.is_staff):
        raise Http404("Not authroized to edit other users quizzes.")

    return render(request, 'frontend/quiz_edit.html')

@login_required
def quiz_create(request):
    return render(request, 'frontend/quiz_creation.html')

def quiz_detail(request, pk):
    try:
        quiz = Quiz.objects.get(pk=pk)
    except Quiz.DoesNotExist:
        raise Http404("Quiz does not exist.")

    if(request.user.is_authenticated or (not quiz.is_private and quiz.is_previewable)):
        return  render(request, 'frontend/quiz_detail.html')
    else:
        return Http404("You need to login to access this quiz.")
        
@login_required
def quiz_take(request, pk):
    try:
        quiz = Quiz.objects.get(pk=pk)
    except Quiz.DoesNotExist:
        raise Http404("Quiz does not exist.")

    return render(request, 'frontend/quiz_take.html')