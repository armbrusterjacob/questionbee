from django.contrib import admin
from quiz.models import Quiz, Choice, Question

class QuestionInLine(admin.TabularInline):
    model = Question
    extra = 2
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 2

class QuizAdmin(admin.ModelAdmin):
    search_fields = ['quiz_name']
    list_display = ('quiz_name', 'created_at', 'last_modified', 'owner')
    # fieldsets = [
    #     (None,               {'fields': ['question_text']}),
    #     ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    # ]
    inlines = [QuestionInLine]

class QuestionAdmin(admin.ModelAdmin):
    search_fields = ['text']
    list_display = ('text', 'question_type')
    inlines = [ChoiceInline]


admin.site.register(Quiz, QuizAdmin)
admin.site.register(Question, QuestionAdmin)