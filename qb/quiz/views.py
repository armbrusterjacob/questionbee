import json

from django.contrib import messages
from django.shortcuts import render
from django.http import Http404
from django.contrib.auth.models import User
from quiz.models import Quiz, Question, Choice, Grade
import quiz.serializers as qs

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view

class GradeList(APIView):
    def get(self, request, format=None):
        grades = Grade.objects.all()
        serializer = qs.GradeSerializer(grades, many=True)
        return Response(serializer.data)

class GradeView(APIView):
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            return Grade.objects.get(pk=pk)
        except Grade.DoesNotExist:
            raise Http404

    def get_quiz(self, pk):
        try:
            return Quiz.objects.get(pk=pk)
        except Quiz.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        grade = self.get_object(pk)

        if(request.user.id != grade.user.id and not request.user.is_staff):
            raise Http404

        quiz = self.get_quiz(grade.quiz.id)
        if(not quiz.can_view_results):
            print(1)
            grade_serializer = qs.GradeSerializerAnswerless(grade)
        elif(not quiz.can_view_answers):
            print(2)
            grade_serializer = qs.GradeSerializer(grade)
        else:
            print(3)
            grade_serializer = qs.GradeSerializerAnswerkey(grade)

        return Response(grade_serializer.data)

    def post(self, request, pk, format=None):
        try:
            quiz = Quiz.objects.get(pk=pk)
        except Quiz.DoesNotExist:
            raise Http404
        
        quiz_serialized = qs.FullQuizSerializer(quiz)

        total_score = 0
        score = 0

        for question in quiz_serialized.data['questions']:
            total_score += 1
            r_index = next(i for i, v in enumerate(request.data) if v['id'] == question['id'])
            chosen = request.data[r_index]

            if(question['question_type'] == "MC"):
                c_index = next(i for i, v in enumerate(question['choices']) if v['is_correct'])
                correct_answer = question['choices'][c_index]
                if(str(chosen['choice']) == str(correct_answer['id'])):
                    score += 1

            if(question['question_type'] == "FI"):
                correct_list = map(lambda a: a['text'], question['choices'])
                correct_set = set(correct_list)
                if(chosen['choice'] in correct_set):
                    score += 1

            if(question['question_type'] == "CA"):
                correct_list = filter(lambda a: a['is_correct'], question['choices'])
                correct_map = map(lambda a: str(a['id']), correct_list)
                chosen_list = chosen['choice']
                if(set(correct_map) == set(chosen_list)):
                    score += 1
            if(question['question_type'] == "LT"):
                total_score -= 1

        answers_json = json.dumps({"answers": request.data,})

        formated_obj = {
            "quiz": pk,
            "user": request.user.id,
            "answers": answers_json,
            "score": score,
            "total_score": total_score,
        }

        grade_serailzier = qs.GradeSubmitSerializer(data=formated_obj)

        if(not grade_serailzier.is_valid()):
            print(grade_serailzier.errors)
            return Response(grade_serailzier.errors, status=status.HTTP_400_BAD_REQUEST)
        grade_serailzier.save()

        

        return Response(grade_serailzier.data, status=status.HTTP_201_CREATED)

class UnfilteredQuizList(APIView):
    """
    List all quizes regardless of privacy setting, available only to staff
    """
    def get(self, request, format=None):
        if(request.user.is_staff):
            return Http404
        quizes = Quiz.objects.all()
        serializer = qs.QuizOnlySerializer(quizes, many=True)
        return Response(serializer.data)

class QuizList(APIView):
    """
    List all quizes and creates them as well as the questions and choices they contain
    """
    def get(self, request, format=None):
        quizzes = Quiz.objects.filter(is_private=False)
        serializer = qs.QuizOnlySerializer(quizzes, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        questions = request.data["questions"]
        del request.data["questions"]
        request.data["owner"] = request.user.id
        if not request.data['can_view_results']:
            request.data['can_view_answers'] = False

        quiz_serializer = qs.QuizSerializer(data=request.data)

        if(not quiz_serializer.is_valid()):
            print(quiz_serializer.errors)
            return Response(quiz_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        quiz_data = quiz_serializer.save()

        for q in questions:
            

            choices = q["choices"]
            del q["choices"]
            q["quiz"] = quiz_data.id
            question_serializer = qs.QuestionSerializer(data=q)
            if(not question_serializer.is_valid()):
                print(question_serializer.errors)
                return Response(question_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            question_data = question_serializer.save()

            
            # Skip choices for long text / essay questions
            if(question_data.question_type == "LT"):
                continue

            found_correct_answer = False

            for c in choices:
                # Make sure only the first correct choice in a multiple choice question is marked correct
                # to prevent multiple choices from being right
                if(question_data.question_type == "MC"):
                    if found_correct_answer:
                        c['is_correct'] = False
                    elif c['is_correct']:
                        found_correct_answer = True

                # Set all fill-in question choices to be correct
                if(question_data.question_type == "FI"):
                    print("set true")
                    c['is_correct'] = True

                        
                c['question'] = question_data.id
                choice_serializer = qs.FullChoiceSerializer(data=c)
                if(not choice_serializer.is_valid()):
                    print(choice_serializer.errors)
                    return Response(choice_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                choice_serializer.save()
        
        messeage_str = "\'" + request.data['quiz_name'] + "\' succesfully created."
        messages.success(request, messeage_str, extra_tags="primary", fail_silently=True)
        quiz_serializer.data['id'] = quiz_data.id

        return Response(quiz_serializer.data, status=status.HTTP_201_CREATED)

class UserDetail(APIView):
    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        user = self.get_object(pk)
        if(request.user.id == user.id or request.user.is_staff):
            serializer = qs.PrivateUserSerializer(user)
        else:
            print("hi")
            serializer = qs.PublicUserSerializer(user)
        return Response(serializer.data)

class QuizDetail(APIView):
    """
    Get, update, or delete an individual quiz instance
    """
    
    def get_object(self, pk):
        try:
            return Quiz.objects.get(pk=pk)
        except Quiz.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        quiz = self.get_object(pk)

        if(quiz.owner.id != request.user.id and not request.user.is_staff):
            serializer = qs.QuizSerializer(quiz)
            data = serializer.data
            # Don't return the 'text' field for all quesitons that are of type "fillin"
            for question in data['questions']:
                if(question['question_type'] == "FI"):
                    for choice in question['choices']:
                        del choice['text']

            return Response(data)
        else:
            serializer = qs.FullQuizSerializer(quiz)
            return Response(serializer.data)

    def put(self, request, pk, format=None):
        orig_quiz = self.get_object(pk)

        if(orig_quiz.owner.id != request.user.id and not request.user.is_staff):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        questions = request.data["questions"]
        del request.data["questions"]
        request.data["owner"] = orig_quiz.owner.id
        quiz_serializer = qs.QuizSerializer(orig_quiz, data=request.data)

        if(not quiz_serializer.is_valid()):
            print(quiz_serializer.errors)
            return Response(quiz_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        quiz_data = quiz_serializer.save()

        # Cascade should delete their respective choices as well
        for o_q in orig_quiz.questions.all():
            o_q.delete()

        for q in questions:
            choices = q["choices"]
            del q["choices"]
            q["quiz"] = quiz_data.id
            question_serializer = qs.QuestionSerializer(data=q)
            if(not question_serializer.is_valid()):
                print(question_serializer.errors)
                return Response(question_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            question_data = question_serializer.save()

            found_correct_answer = False

            for c in choices:
                # Make sure only the first correct choice in a multiple choice question is marked correct
                # to prevent multiple choices from being right
                if(question_data.question_type == "MC"):
                    if found_correct_answer:
                        c['is_correct'] = False
                    elif c['is_correct']:
                        found_correct_answer = True

                # Set all fill-in question choices to be correct
                if(question_data.question_type == "FI"):
                    print("set true")
                    c['is_correct'] = True

                c['question'] = question_data.id
                choice_serializer = qs.FullChoiceSerializer(data=c)
                if(not choice_serializer.is_valid()):
                    print(choice_serializer.errors)
                    return Response(choice_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                choice_serializer.save()

        messeage_str = "\'" + str(request.data['quiz_name']) + "\' succesfully updated."
        messages.success(request, messeage_str, extra_tags="primary", fail_silently=True)

        return Response(quiz_serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, pk, format=None):
        quiz = self.get_object(pk)
        if(quiz.owner.id != request.user.id and not request.user.is_staff):
            return Response(status=status.HTTP_400_BAD_REQUEST)
        quiz.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
