from rest_framework import serializers
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from quiz.models import Quiz, Question, Choice, Grade
from django.db import models

class ChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = ['text', 'id']

class FullChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choice
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    choices = ChoiceSerializer(many=True, required=False)

    class Meta:
        model = Question
        fields = '__all__'

class FullQuestionSerializer(serializers.ModelSerializer):
    choices = FullChoiceSerializer(many=True, required=False)

    class Meta:
        model = Question
        fields = '__all__'


class QuizOnlySerializer(serializers.ModelSerializer):
    author = serializers.CharField()
    class Meta:
        model = Quiz
        fields = ['id', 'quiz_name', 'created_at', 'last_modified', 'author', 'owner', 'can_view_answers', 'can_view_correct_answers', 'can_view_results']
        extra_kwargs = {'questions': {'required':False}}

class QuizSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, required=False)

    class Meta:
        model = Quiz
        fields = '__all__'
        extra_kwargs = {'questions': {'required':False}}

class FullQuizSerializer(serializers.ModelSerializer):
    questions = FullQuestionSerializer(many=True, required=False)

    class Meta:
        model = Quiz
        fields = '__all__'
        extra_kwargs = {'questions': {'required':False}}


class GradeSerializerAnswerless(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = ['id', 'quiz', 'score', 'total_score', 'user']

class GradeSubmitSerializer(serializers.ModelSerializer):
    answers = serializers.JSONField()
    class Meta:
        model = Grade
        fields = '__all__'

class GradeSerializer(serializers.ModelSerializer):
    quiz = QuizSerializer()
    answers = serializers.JSONField()
    class Meta:
        model = Grade
        fields = '__all__'

class GradeSerializerAnswerkey(serializers.ModelSerializer):
    quiz = FullQuizSerializer(required=True)
    class Meta:
        model = Grade
        fields = '__all__'


class PublicUserSerializer(serializers.ModelSerializer):
    quizzes = QuizOnlySerializer(many=True, required=False)
    class Meta:
        model = User
        fields = ['username', 'quizzes']

class PrivateUserSerializer(serializers.ModelSerializer):
    quizzes = QuizOnlySerializer(many=True, required=False)
    grades = GradeSerializerAnswerless(many=True, required=False)
    class Meta:
        model = User
        fields = ['username', 'quizzes', 'grades']