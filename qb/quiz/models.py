from django.db import models
from django.contrib.postgres.fields import JSONField
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User

class Quiz(models.Model):
    """
    Quizes are owned by users and contain a list of quesitons
    """
    quiz_name = models.CharField(default="Quiz Name", max_length=100)
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True, auto_now_add=False)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="quizzes",
        on_delete=models.CASCADE,
    )

    is_private = models.BooleanField(default=False, null=False)
    is_previewable = models.BooleanField(default=True, null=False)
    can_view_results = models.BooleanField(default=True, null=False)
    can_view_answers = models.BooleanField(default=False, null=False)

    def author(self):
        """
        Return username of owner
        """
        return self.owner.username

    def __str__(self):
        return self.quiz_name
    def __unicode__(self):
        return self.quiz_name

class Grade(models.Model):
    quiz = models.ForeignKey(Quiz,
        related_name="grades",
        on_delete=models.SET_NULL,
        null=True 
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="grades",
        on_delete=models.CASCADE,
    )
    score = models.FloatField(null=False)
    total_score = models.FloatField(null=False, default=1)
    answers = JSONField(encoder="")
    created_at = models.DateTimeField(auto_now=False, auto_now_add=True)

    def score_percent(self):
        total = self.total_score
        if total != 0:
            total = self.score / total * 100
        return ('%.15f' % total).rstrip('0').rstrip('.') 


    def __str__(self):
        return str(self.quiz)
    def __unicode__(self):
        return str(self.quiz)

class Question(models.Model):
    """
    Questions come in many types. Multiple Choice and Choose all each user a series of choices,
    but only choose all has multiple. Fillin can have many correct answers but no incorrect ones.
    Long text have no preset answers and need to be graded individually.
    """
    quiz =  models.ForeignKey(Quiz, related_name="questions", on_delete=models.CASCADE)
    text = models.CharField(default="Quetsion", max_length=500)

    MULTI = 'MC'
    CHOOSEALL = "CA"
    FILLIN = 'FI'
    LONGTEXT = 'LT'
    QUESTION_CHOICES = (
        (MULTI, 'Multiple Choice'),
        (FILLIN, 'Fill-in the Blank'),
        (LONGTEXT, 'Long Text Form'),
        (CHOOSEALL, 'Choose all that apply'),
    )
    question_type = models.CharField(
        max_length=2,
        choices=QUESTION_CHOICES,
        default=MULTI,
    )

    def __unicode__(self):
        return str(self.quiz)[0:10] + " | " + self.text
    def __str__(self):
        return str(self.quiz)[0:10] + " | " + self.text

class Choice(models.Model):
    """
    Chioces are the the possibles ansers a user can choose from.
    """
    question =  models.ForeignKey(Question, related_name="choices", on_delete=models.CASCADE)
    text = models.CharField(default="Choice", max_length=200)
    is_correct = models.BooleanField(default=True)

    def __unicode__(self):
        return self.text
    def __str__(self):
        return self.text

