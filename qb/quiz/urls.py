from django.urls import path
# from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('api/quiz/', views.QuizList().as_view(), name="quiz"),
    path('api/quiz/all', views.UnfilteredQuizList().as_view(), name="quiz-all"),
    path('api/quiz/<int:pk>/', views.QuizDetail().as_view(), name="quiz-detail"),

    path('api/grade/', views.GradeList.as_view(), name="grade"),
    path('api/grade/<int:pk>/', views.GradeView().as_view(), name="grade-detail"),

    path('api/user/<int:pk>/', views.UserDetail.as_view(), name="user-detail"),
]

# urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])