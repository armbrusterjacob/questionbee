# Generated by Django 2.2.6 on 2019-11-07 04:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0008_grade_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grade',
            name='quiz',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='quizzes', to='quiz.Quiz'),
        ),
    ]
