# Generated by Django 2.2.6 on 2019-10-22 23:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('quiz', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='quiz',
            old_name='Quiz Name',
            new_name='quiz_name',
        ),
    ]
